function myMap() {
	var myCenter = new google.maps.LatLng(53.925391, 27.638406);
	var mapCanvas = document.querySelector(".map");
	var mapOptions = {center: myCenter, zoom: 13};
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({position:myCenter});
	marker.setMap(map);
}