$(document).on('ready', function() {
	$(".slider").slick({
		dots: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		speed: 1000,
		fade: true,
		arrows: false
	});
	$(".what_they_say_slider").slick({
		dots: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
		fade: true,
        nextArrow: '<div class="right_arrow"><img src="images/what_they_say/what_they_say_sprites/right_arrow.png" alt=""></div>',
        prevArrow: '<div class="left_arrow"><img src="images/what_they_say/what_they_say_sprites/left_arrow.png" alt=""></div>'
	});
	$('[data-toggle="tooltip"]').tooltip(
		{title: "<h4><b>AL RAYHAN</b> / UI Designer</h4><p>Lorem Ipsum is not simply is  an action designer random text It has roots in a piece.</p><p><ul class='center-block list-inline'><li class='icon-facebook'></li><li class='icon-twitter'></li><li class='icon-ball'></li><li class='icon-mail'></li></ul></p>",
		html: true,
		placement: "bottom", animation: true});
	
});